**Elastic Beanstalk with Docker Single Instance & Let's Encrypt SSL**

---

## Setup Elasticbeanstalk Environment

First, add an environment variable for create a domain certificate SSL.

1. Go to your EBS environment application
2. Select a Configuration -> Software
3. Click **Modify**
4. Input this Environment Properties at the bottom
	- **CERTDOMAIN** : your.domain.com or your-app.ap-southeast-xx.elasticbeanstalk.com
	- **EMAIL** : your@email.com
5. Apply for save the environment variable
6. This variable will only be read by .ebextentions

---

## Usage

Zip all this required file into application.zip and upload or deploy into elasticbeanstalk

**zip -r application.zip .ebignore .ebextenstions Dockerrun.aws.json**